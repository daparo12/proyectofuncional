-module(proyecto).
-export([union/2, pullNodes/1, population/2, cross/2, mutate/2, mutateaux/3,replaceat/3, server/5, genetico/5,colorList/2, select/2, edgeCompare/2, edgeCompare/4, collision/3, generahilos/5, mejores/1, pob/3,hijos/5]).

-module(proyecto).
-export([union/2, pullNodes/1, population/2, cross/2, mutate/2, mutateaux/3,replaceat/3, genetico/3,colorList/2, select/2, edgeCompare/2, edgeCompare/4, collision/3, mejores/1, pob/3,hijos/6]).


%%proyecto:server([[a,b],[b,c],[c,d],[d,a]],4,-1,3,[]).
%%server

%server(G,C,-1,N,P)->PI= population(G,C),generahilos(2,self(),PI,G,C), server(G,C,12,N,P);
%server(G,C,0,N,P)-> PI= mejores(P), generahilos(2,self(),PI,G,C), server(G,C,12,N-1,[]);
%server(G,C,I,N,P)->
    %receive
     %   {PI,PB}-> io:format("llega al server"),server(G,C,I-1,N,union(PB,P));
	%{sol,Sol}-> io:format(Sol)
    %end.

%generahilos(0, Server,PI,G,C)->io:format("Escoger mejores");
%generahilos(Can,Server,PI,G,C)-> spawn(fun()->genetico(0.05,G,C,PI,3)end), generahilos(Can-1,Server,PI,G,C).




%%Unir lista

union(E,[])->[E];
union(E,[E|H])->[E|H];
union(E,[H|T])->[H|union(E,T)].

%%Sacar nodos

pullNodes(L)->pullNodes(L,[]).

pullNodes([],L)->L;
pullNodes([[A,B]|X],L)->pullNodes(X,union(A,union(B,L))).

%%Asignar color a los nodos(tuplas).
%%Poblacion inicial

population(L,C)->population(pullNodes(L),C,C*length(L)-C,[]).

population(_L,_C,0,Pop)-> Pop;
population(L,C,N,Pop)-> lists:append([lists:map(fun(X)->{X,rand:uniform(C)}end,L)],Pop)++population(L,C,N-1,Pop).

%%Cruzar poblacion

cross(P1,P2)-> lists:sublist(P1,round(length(P1)/2)) ++ lists:sublist(P2,round(length(P1)/2)+1,length(P2)).

%%Mutar

mutate(Child,C)-> mutateaux(Child,C,rand:uniform(length(Child))).
mutateaux(Child,C,Index)-> replaceat(Child,Index,setelement(2, lists:nth(Index,Child), rand:uniform(C))).

replaceat(L,Index,NewValue) -> {L1,[_|L2]} = lists:split(Index-1,L), L1++[NewValue|L2].

%Mejores
mejores([[H|T]|A])-> mejores([[H|T]|A],round(length([[H|T]|A])*0.2)).

mejores([],_PE)->[];
mejores([[H|T]|A],PE)->lists:sublist(lists:sort([doble([H|T])] ++ mejores(A,PE)),PE).


%Colision al cuadrado
doble([])->[];
doble([H|T])->[lists:nth(1,[H|T])*lists:nth(1,[H|T]),lists:nth(2,[H|T])].

%%Validar colisiones 

%%Dominio:Solución, lista de nodos, cantidad de colores. 
collision(Child,L,C)-> [edgeCompare(L,colorList(C,Child)), Child].

%%Crear listas de colores
%%Dominio: Cantidad de colores, solucion
colorList(C,Child)-> colorList(C,[],Child).

%%Dominio: Cantidad de colores, lista de colores, solucion
colorList(0,R,_)-> lists:reverse(R);
colorList(C,R,Child)-> colorList(C-1,lists:append(R,[select(C,Child)]),Child).

%%Crea grupos por color
select(X,L)-> [Y || {Y, X1} <-L, X == X1].

%%Comparar Aristas con Colores
%%Dominio: Lista de nodos, lista de colores
edgeCompare(L,LC)-> edgeCompare(L,LC,length(LC),0).

edgeCompare([],_,_,R)-> R;
edgeCompare([_|T],LC,0,R)-> edgeCompare(T,LC,length(LC),R);
edgeCompare([H|T],LC,C,R)->
	A = lists:member(lists:nth(1,H),lists:nth(C,LC)),
	B = lists:member(lists:nth(2,H),lists:nth(C,LC)),
	if
	A and B->
		edgeCompare(T,LC,length(LC),R+1);
	true-> %%else
		edgeCompare([H|T],LC,C-1,R)
	end.

pob(PI,G,C)->pob(PI,G,C,[],length(PI)).

pob([],_G,_C,Acum,0)->Acum;
pob([H|T],G,C,Acum,N)->[lists:append(collision(H,G,C),Acum)]++pob(T,G,C,Acum,N-1).

 

%%Genetico
genetico(G,C,R)->genetico(0.5,G,C,population(G,C),R).
genetico(_PM,_G,_C,PB,0)-> lists:nth(1,PB);
genetico(PM,G,C,PI,R)-> 
	PT=pob(PI,G,C),
	M= mejores(PT),
	Nhijos=	length(PI)-1,
	SigGen=[lists:nth(2,lists:nth(1,M))],
	PB=hijos(Nhijos,PM,PI,C,SigGen,G),
	genetico(PM,G,C,PB,R-1).
	

hijos(N,PM,PI,C,SigGen,G)-> 
	PA= lists:nth(rand:uniform(length(PI)),PI),
	PB= lists:nth(rand:uniform(length(PI)),PI),
	hijos(N,PM,PI,C,SigGen,cross(PA,PB),G).


hijos(0,_PM,_PI,_C,SigGen,_X,_G)->SigGen;
hijos(N,PM,PI,C,SigGen,X,G)-> 
	   		P= rand:uniform(20)/100,
	   		if
	     			P =< PM-> 
														hijos(N-1,PM,PI,C,lists:append(SigGen,[X]),mutate(X,C),G);
				true->
					hijos(N-1,PM,PI,C,lists:append(SigGen,[X]),X,G)	

	   		end.
			





























